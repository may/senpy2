use std::{
    collections::{HashMap, HashSet},
    fs,
    io::Write,
    path::Path,
};

use clap::Parser;

mod compiler;
mod lexer;
mod parser;

#[derive(Parser, Debug)]
struct Args {
    #[arg(short, long)]
    source: String,
    #[arg(short, long)]
    output: String,
    #[arg(short, long)]
    include: Vec<String>,
    #[arg(short, long)]
    asset: Vec<String>,
}

fn main() {
    let args = Args::parse();

    let mut assets = HashMap::new();
    let mut asset_set = HashSet::new();
    for asset in &args.asset {
        let path = Path::new(asset);
        if path.exists() {
            let file_name = path.file_name().unwrap().to_str().unwrap();
            assets.insert(file_name.to_string(), asset.to_string());
            asset_set.insert(file_name.to_string());
        } else {
            panic!("Asset file not found: {}", asset)
        }
    }

    let mut includes = HashMap::new();
    for script in &args.include {
        let path = Path::new(script);
        if path.exists() {
            let file_name = path.file_name().unwrap().to_str().unwrap();
            includes.insert(file_name.to_string(), script.to_string());
        } else {
            panic!("Source script not found: {}", script)
        }
    }

    let Ok(text) = fs::read_to_string(&args.source) else {
        panic!("Source file not found: {}", args.source)
    };

    let output_folder = Path::new(&args.output);
    if !output_folder.exists() {
        panic!("Output folder not found: {}", args.output)
    } else if !output_folder.is_dir() {
        panic!("Output must be a folder");
    }

    let tokens = lexer::lex(&text).unwrap();
    let ast = parser::parse(tokens).unwrap();
    let program = compiler::compile(ast, includes, asset_set).unwrap();

    let game_path = output_folder.join("game");
    let data_path = output_folder.join("data");
    let assets_folder = output_folder.join("assets");

    fs::remove_dir_all(output_folder).unwrap();
    fs::create_dir(output_folder).unwrap();

    let mut game_file = fs::File::create(game_path).unwrap();
    let mut game_bytes = vec![];
    program.instructions.iter().for_each(|x| {
        game_bytes.extend(x.to_bytes());
    });
    game_file.write_all(&game_bytes).unwrap();

    let mut data_file = fs::File::create(data_path).unwrap();
    data_file.write_all(&program.data).unwrap();

    fs::create_dir(&assets_folder).unwrap();
    for (sym_asset, asset) in assets.iter() {
        let asset_path = assets_folder.join(sym_asset);
        let asset_bytes = fs::read(asset).unwrap();
        let mut asset_file = fs::File::create(asset_path).unwrap();
        asset_file.write_all(&asset_bytes).unwrap();
    }
}

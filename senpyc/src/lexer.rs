#[derive(Debug)]
pub enum LexingError {}

#[derive(Debug, Clone)]
pub enum Token {
    OpenBracket,
    CloseBracket,
    StringLiteral(String),
    IntegerLiteral(i64),
    FloatLiteral(f64),
    FunctionCall(String),
    MacroInvocation(String),
    AssetReference(String),
    Include(String),
}

fn build_until(chars: &[char], index: &mut usize, predicate: impl Fn(char) -> bool) -> String {
    let mut builder = String::new();
    let mut current_char = chars[*index];
    while !predicate(current_char) {
        builder.push(current_char);
        *index += 1;
        current_char = chars[*index];
    }
    builder
}

pub fn lex(text: &str) -> Result<Vec<Token>, LexingError> {
    let mut tokens = vec![];
    let mut index = 0;
    let chars = text.chars().collect::<Vec<_>>();

    while let Some(chr) = chars.get(index) {
        match chr {
            '[' => {
                tokens.push(Token::OpenBracket);
                index += 1
            }
            ']' => {
                tokens.push(Token::CloseBracket);
                index += 1
            }
            ' ' | '\n' | '\t' => index += 1,
            '"' => {
                index += 1;
                let result = build_until(&chars, &mut index, |x| x == '"');
                tokens.push(Token::StringLiteral(result));
                index += 1;
            }
            '`' => {
                index += 1;
                let result = build_until(&chars, &mut index, |x| x == '`');
                tokens.push(Token::StringLiteral(result));
                index += 1;
            }
            '0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9' => {
                let result = build_until(&chars, &mut index, |x| !x.is_ascii_digit() && x != '.');
                if result.chars().any(|x| x == '.') {
                    let number = result.parse::<f64>().unwrap();
                    tokens.push(Token::FloatLiteral(number))
                } else {
                    let number = result.parse::<i64>().unwrap();
                    tokens.push(Token::IntegerLiteral(number))
                }
            }
            '#' => {
                index += 1;
                let result = build_until(&chars, &mut index, |x| matches!(x, ' ' | '\n' | '\t'));
                tokens.push(Token::MacroInvocation(result));
            }
            '+' => {
                index += 1;
                let result = build_until(&chars, &mut index, |x| matches!(x, ' ' | '\n' | '\t'));
                tokens.push(Token::AssetReference(result));
            }
            '*' => {
                index += 1;
                let result = build_until(&chars, &mut index, |x| matches!(x, ' ' | '\n' | '\t'));
                tokens.push(Token::Include(result));
            }
            _ => {
                let result = build_until(&chars, &mut index, |x| matches!(x, ' ' | '\n' | '\t'));
                tokens.push(Token::FunctionCall(result));
            }
        }
    }

    Ok(tokens)
}

use crate::lexer::Token;

#[derive(Debug)]
pub enum ParsingError {}

#[derive(Debug)]
pub enum Node {
    CreateFunction(Vec<Node>),
    PushString(String),
    PushInteger(i64),
    PushFloat(f64),
    CallFunction(String),
    InvokeMacro(String),
    EmbedAsset(String),
    IncludeScript(String),
}

pub fn parse(tokens: Vec<Token>) -> Result<Vec<Node>, ParsingError> {
    let mut result = vec![];
    let mut builder = vec![];
    let mut level = 0;

    for token in tokens {
        match token {
            Token::OpenBracket => {
                level += 1;
                if level != 1 {
                    builder.push(token);
                }
            }
            Token::CloseBracket => {
                level -= 1;
                if level == 0 {
                    let inner_node = parse(builder)?;
                    result.push(Node::CreateFunction(inner_node));
                    builder = vec![];
                } else {
                    builder.push(token);
                }
            }
            Token::StringLiteral(_) => {
                if level == 0 {
                    let Token::StringLiteral(string) = token else { unreachable!() };
                    result.push(Node::PushString(string));
                } else {
                    builder.push(token);
                }
            }
            Token::IntegerLiteral(number) => {
                if level == 0 {
                    result.push(Node::PushInteger(number));
                } else {
                    builder.push(token);
                }
            }
            Token::FloatLiteral(number) => {
                if level == 0 {
                    result.push(Node::PushFloat(number));
                } else {
                    builder.push(token);
                }
            }
            Token::FunctionCall(_) => {
                if level == 0 {
                    let Token::FunctionCall(function) = token else { unreachable!() };
                    result.push(Node::CallFunction(function));
                } else {
                    builder.push(token);
                }
            }
            Token::MacroInvocation(_) => {
                if level == 0 {
                    let Token::MacroInvocation(name) = token else { unreachable!() };
                    result.push(Node::InvokeMacro(name));
                } else {
                    builder.push(token);
                }
            }
            Token::AssetReference(_) => {
                if level == 0 {
                    let Token::AssetReference(path) = token else { unreachable!() };
                    result.push(Node::EmbedAsset(path));
                } else {
                    builder.push(token);
                }
            }
            Token::Include(_) => {
                if level == 0 {
                    let Token::Include(path) = token else { unreachable!() };
                    result.push(Node::IncludeScript(path));
                } else {
                    builder.push(token);
                }
            }
        }
    }

    Ok(result)
}

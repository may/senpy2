use std::{
    collections::{HashMap, HashSet},
    fs,
};

use bytecode::Instruction;

use crate::parser::Node;

#[derive(Debug)]
pub enum CompilingError {
    UnknownMacro(String),
    AssetNotFound(String),
    ScriptNotFound(String),
}

struct Compiler {
    instructions: Vec<Instruction>,
    data: Vec<u8>,
    assets: Vec<String>,
    function_stack: Vec<usize>,
    asset_set: HashSet<String>,
    include_map: HashMap<String, String>,
}

#[derive(Debug, Clone)]
pub struct Program {
    pub instructions: Vec<Instruction>,
    pub data: Vec<u8>,
    pub assets: Vec<String>,
}

impl Compiler {
    fn new(includes: HashMap<String, String>, assets: HashSet<String>) -> Self {
        Self {
            instructions: vec![],
            data: vec![],
            assets: vec![],
            function_stack: vec![],
            include_map: includes,
            asset_set: assets,
        }
    }

    fn embed_string(&mut self, string: &str) -> (u64, u64) {
        let data_pointer = self.data.len() as u64;
        let data_length = string.len() as u64;
        self.data.extend(string.as_bytes());
        (data_pointer, data_length)
    }

    fn invoke_macro(&mut self, name: String) -> Result<(), CompilingError> {
        match name.as_str() {
            "menu" => {
                let instructions = ["save_stack_length", "call", "menu", "get"];
                for instruction in instructions {
                    let (pointer, length) = self.embed_string(instruction);
                    self.instructions
                        .push(Instruction::CallFunction(pointer, length));
                }
            }
            _ => return Err(CompilingError::UnknownMacro(name)),
        }
        Ok(())
    }

    fn compile(&mut self, nodes: Vec<Node>) -> Result<(), CompilingError> {
        for node in nodes {
            match node {
                Node::PushFloat(number) => self.instructions.push(Instruction::PushFloat(number)),
                Node::PushInteger(number) => self.instructions.push(Instruction::PushInt(number)),
                Node::PushString(string) => {
                    let (pointer, length) = self.embed_string(&string);
                    self.instructions
                        .push(Instruction::PushString(pointer, length));
                }
                Node::CallFunction(name) => {
                    let (pointer, length) = self.embed_string(&name);
                    self.instructions
                        .push(Instruction::CallFunction(pointer, length));
                }
                Node::CreateFunction(inner_nodes) => {
                    self.instructions.push(Instruction::StartFunction(0));
                    let start_function_pointer = self.instructions.len();
                    self.function_stack.push(start_function_pointer);
                    self.compile(inner_nodes)?;
                    self.instructions.push(Instruction::EndFunction);
                    let end_function_pointer = self.instructions.len();
                    self.instructions[start_function_pointer - 1] =
                        Instruction::StartFunction(end_function_pointer as u64);
                    let pointer = self.function_stack.pop().unwrap() as u64;
                    self.instructions
                        .push(Instruction::PushFunctionPointer(pointer));
                }
                Node::EmbedAsset(sym_path) => {
                    if self.asset_set.contains(&sym_path) {
                        let (pointer, length) = self.embed_string(&sym_path);
                        self.instructions.push(Instruction::Asset(pointer, length));
                        self.assets.push(sym_path);
                    } else {
                        return Err(CompilingError::AssetNotFound(sym_path));
                    }
                }
                Node::IncludeScript(sym_path) => {
                    let path_option = self.include_map.get(&sym_path);
                    let owned_option = path_option.map(|x| x.to_owned());
                    if let Some(script_path) = owned_option {
                        let text = fs::read_to_string(script_path).unwrap();
                        let tokens = crate::lexer::lex(&text).unwrap();
                        let nodes = crate::parser::parse(tokens).unwrap();
                        self.compile(nodes)?;
                    } else {
                        return Err(CompilingError::ScriptNotFound(sym_path));
                    }
                }
                Node::InvokeMacro(name) => {
                    self.invoke_macro(name)?;
                }
            }
        }
        Ok(())
    }
}

pub fn compile(
    nodes: Vec<Node>,
    includes: HashMap<String, String>,
    assets: HashSet<String>,
) -> Result<Program, CompilingError> {
    let mut compiler = Compiler::new(includes, assets);
    compiler.compile(nodes)?;
    Ok(Program {
        instructions: compiler.instructions,
        data: compiler.data,
        assets: compiler.assets,
    })
}

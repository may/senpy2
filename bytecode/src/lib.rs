#[derive(Debug, Clone, Copy)]
pub enum Instruction {
    PushString(u64, u64),
    CallFunction(u64, u64),
    PushInt(i64),
    PushFloat(f64),
    PushFunctionPointer(u64),
    Asset(u64, u64),
    StartFunction(u64),
    EndFunction,
}

macro_rules! combine {
    ( $( $x:expr ),* ) => {{
        let mut result = Vec::new();
        $(
            result.extend($x);
        )*
        result
    }};
}

macro_rules! from_slice {
    ( $target:ident, $bytes:expr ) => {{
        let mut array = [0u8; 8];
        array.copy_from_slice(&$bytes);
        $target::from_le_bytes(array)
    }};
}

impl Instruction {
    pub fn to_bytes(&self) -> Vec<u8> {
        match self {
            Instruction::EndFunction => combine!(vec![0x01], vec![0x00; 16]),
            Instruction::StartFunction(pointer) => {
                combine!(vec![0x02], pointer.to_le_bytes(), vec![0x00; 8])
            }
            Instruction::PushFunctionPointer(pointer) => {
                combine!(vec![0x03], pointer.to_le_bytes(), vec![0x00; 8])
            }
            Instruction::PushFloat(number) => {
                combine!(vec![0x04], number.to_le_bytes(), vec![0x00; 8])
            }
            Instruction::PushInt(number) => {
                combine!(vec![0x05], number.to_le_bytes(), vec![0x00; 8])
            }
            Instruction::Asset(pointer, length) => {
                combine!(vec![0x06], pointer.to_le_bytes(), length.to_le_bytes())
            }
            Instruction::CallFunction(pointer, length) => {
                combine!(vec![0x07], pointer.to_le_bytes(), length.to_le_bytes())
            }
            Instruction::PushString(pointer, length) => {
                combine!(vec![0x08], pointer.to_le_bytes(), length.to_le_bytes())
            }
        }
    }
    pub fn from_bytes(bytes: &[u8]) -> Instruction {
        match bytes[0] {
            0x01 => Instruction::EndFunction,
            0x02 => Instruction::StartFunction(from_slice!(u64, bytes[1..9])),
            0x03 => Instruction::PushFunctionPointer(from_slice!(u64, bytes[1..9])),
            0x04 => Instruction::PushFloat(from_slice!(f64, bytes[1..9])),
            0x05 => Instruction::PushInt(from_slice!(i64, bytes[1..9])),
            0x06 => Instruction::Asset(
                from_slice!(u64, bytes[1..9]),
                from_slice!(u64, bytes[9..17]),
            ),
            0x07 => Instruction::CallFunction(
                from_slice!(u64, bytes[1..9]),
                from_slice!(u64, bytes[9..17]),
            ),
            0x08 => Instruction::PushString(
                from_slice!(u64, bytes[1..9]),
                from_slice!(u64, bytes[9..17]),
            ),
            _ => panic!("Unknown instruction code: {}", bytes[0]),
        }
    }
}

use std::io::{self, Write};

use bytecode::Instruction;
use interchange::{InputEvent, JsonEvent, OutputEvent};

pub enum Value {
    String(String),
    Integer(i64),
    Float(f64),
    Pointer(u64),
}

pub struct Runtime {
    instructions: Vec<Instruction>,
    data: Vec<u8>,
    pc: usize,
    asset_folder: String,
    stack: Vec<Value>,
    call_stack: Vec<usize>,
}

impl Runtime {
    pub fn new(instructions: Vec<Instruction>, data: Vec<u8>, asset_folder: String) -> Self {
        Self {
            instructions,
            pc: 0,
            data,
            asset_folder,
            call_stack: vec![],
            stack: vec![],
        }
    }

    fn resolve_string(&self, pointer: u64, length: u64) -> String {
        let slice = &self.data[pointer as usize..(pointer + length) as usize];
        let result = String::from_utf8_lossy(slice).to_string();
        result
    }

    fn execute(&mut self, instruction: Instruction, event: &str) -> Option<OutputEvent> {
        match instruction {
            Instruction::StartFunction(pointer) => {
                self.pc = (pointer - 1) as usize;
                None
            }
            Instruction::EndFunction => {
                let destination = self.call_stack.pop().unwrap();
                self.pc = destination;
                None
            }
            Instruction::Asset(pointer, length) | Instruction::PushString(pointer, length) => {
                let text = self.resolve_string(pointer, length);
                self.stack.push(Value::String(text));
                None
            }
            Instruction::PushInt(number) => {
                self.stack.push(Value::Integer(number));
                None
            }
            Instruction::PushFloat(number) => {
                self.stack.push(Value::Float(number));
                None
            }
            Instruction::PushFunctionPointer(number) => {
                self.stack.push(Value::Pointer(number));
                None
            }
            Instruction::CallFunction(pointer, length) => {
                let function = self.resolve_string(pointer, length);
                println!("{}", function);
                None
            }
        }
    }

    fn poll(&mut self, event: &str) -> Option<OutputEvent> {
        loop {
            if self.pc == self.instructions.len() {
                return None;
            }
            let instruction = self.instructions[self.pc];
            let response = self.execute(instruction, event);
            self.pc += 1;
            if let Some(output) = response {
                return Some(output);
            }
        }
    }

    pub fn start(&mut self) {
        loop {
            print!(" >> ");
            io::stdout().flush().unwrap();
            let mut input = String::new();
            io::stdin().read_line(&mut input).unwrap();
            if let Some(output) = self.poll(input.trim()) {
                println!("{:?}", output);
            } else {
                return;
            }
        }
    }
}

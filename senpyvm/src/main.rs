use bytecode::Instruction;
use clap::Parser;
use std::{fs, path::Path};

mod runtime;

#[derive(Parser, Debug)]
struct Args {
    #[arg(short, long)]
    game: String,
    #[arg(short, long)]
    data: String,
    #[arg(short, long)]
    assets: String,
}

fn main() {
    let args = Args::parse();

    if !Path::new(&args.game).exists() {
        panic!("Couldn't find game binary: {}", args.game);
    }
    if !Path::new(&args.data).exists() {
        panic!("Couldn't find data binary: {}", args.data);
    }

    let assets = Path::new(&args.assets);
    if !assets.exists() {
        panic!("Couldn't find assets directory: {}", args.assets);
    } else if !assets.is_dir() {
        panic!("Assets must be directory: {}", args.assets);
    }

    let game_bytes = fs::read(&args.game).unwrap();
    let data = fs::read(&args.data).unwrap();
    let instructions = game_bytes
        .chunks(17)
        .map(|x| Instruction::from_bytes(x))
        .collect::<Vec<_>>();

    let mut rt = runtime::Runtime::new(instructions, data, args.assets);
    rt.start();
}

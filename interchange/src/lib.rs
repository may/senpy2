use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug)]
pub enum InputEvent {
    Confirm,
    Text(String),
    Choice(u64),
}

#[derive(Serialize, Deserialize, Debug)]
pub enum OutputEvent {
    Text(String, String),
    Input,
    Menu(Vec<String>),
}

pub trait JsonEvent<'a>: Serialize + Deserialize<'a> {
    fn into_json(&self) -> String {
        serde_json::to_string(self).unwrap()
    }
    fn from_json(text: &'a str) -> Self {
        serde_json::from_str(text).unwrap()
    }
}

impl<'a> JsonEvent<'a> for InputEvent {}
impl<'a> JsonEvent<'a> for OutputEvent {}
